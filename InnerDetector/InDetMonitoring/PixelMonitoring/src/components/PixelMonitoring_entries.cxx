#include "PixelMonitoring/PixelAthClusterMonTool.h"
#include "PixelMonitoring/PixelAthErrorMonTool.h"
#include "PixelMonitoring/PixelAthHitMonTool.h"

DECLARE_COMPONENT( PixelAthClusterMonTool )
DECLARE_COMPONENT( PixelAthErrorMonTool )
DECLARE_COMPONENT( PixelAthHitMonTool )
