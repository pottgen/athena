/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// ParametersT.icc, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

// STD
#include <limits>
#include <utility>
// Trk
#include "TrkEventPrimitives/ParamDefs.h"

namespace Trk
{ 
  namespace
  {
    static constexpr double INVALID=std::numeric_limits<double>::quiet_NaN();
    static constexpr double INVALID_P(10e9);
    static constexpr double INVALID_QOP(10e-9);

    template<typename T>
    int sgn(const T& val)
    {
      return (val > 0) - (val < 0);
    }
  }
  
  // Constructor with local arguments - uses global <-> local for parameters 
  template<int DIM,class T,class S>
  ParametersT<DIM,T,S>::ParametersT(double loc1,
				    double loc2,
				    double phi,
				    double theta,
				    double qop,
				    const S& surface,
				    AmgSymMatrix(DIM)* cov):
    ParametersBase<DIM,T>(),
    m_parameters(),
    m_covariance(cov),
    m_position(),
    m_momentum(), 
    m_surface(nullptr), 
    m_chargeDef(sgn(qop))
  {

    m_surface.reset((surface.isFree() ? surface.clone() : &surface));
    // check qoverp is physical
    double p = 0.;
    if(qop != 0)
      p = fabs(1./qop);
    else
    {
      // qop is unphysical. No momentum measurement.
      p = INVALID_P;
      qop = INVALID_QOP;
    }

    // fill the parameters
    // cppcheck-suppress constStatement
    m_parameters << loc1, loc2, phi, theta, qop;

    // decide the sign of the charge
    // if(qop < 0.)
    //   m_chargeDef->setCharge(-1.);
    // else
    //   m_chargeDef->setCharge(1.);
     
    // now calculate the momentum
    m_momentum = Amg::Vector3D(p*cos(phi)*sin(theta),
			       p*sin(phi)*sin(theta),
			       p*cos(theta));
  
    m_surface->localToGlobal(this->localPosition(),momentum(),m_position);
  }

  // Constructor with local arguments - uses global <-> local for parameters
  template<int DIM,class T,class S>
  ParametersT<DIM,T,S>::ParametersT(const AmgVector(DIM)& parameters,
                                    const S& surface,
                                    AmgSymMatrix(DIM)* cov):
    ParametersBase<DIM,T>(),
    m_parameters(parameters),
    m_covariance(cov),
    m_position(),
    m_momentum(),  
    m_surface(nullptr),
    m_chargeDef(sgn(parameters[Trk::qOverP]))
  {  
    m_surface.reset((surface.isFree() ? surface.clone() : &surface));
    // decide the sign of the charge
    double qop = m_parameters[Trk::qOverP];
    // if(qop < 0.) 
    //   m_chargeDef->setCharge(-1.);
    // else
    //   m_chargeDef->setCharge(1.);
  
    // check qoverp is physical
    double p = 0.;
    if(qop != 0.)
      p = fabs(1./qop);
    else
    {
      // qop is unphysical. No momentum measurement.
      p = INVALID_P;
      qop = INVALID_QOP;
    }
  
    // fill momentum & then position using the surface
    m_momentum = Amg::Vector3D(p*cos(m_parameters[Trk::phi])*sin(m_parameters[Trk::theta]),
			       p*sin(m_parameters[Trk::phi])*sin(m_parameters[Trk::theta]),
			       p*cos(m_parameters[Trk::theta]));

    m_surface->localToGlobal(this->localPosition(),momentum(),m_position);
  }
     
  // Constructor with global arguments - uses global <-> local for parameters */
  template<int DIM,class T,class S>
  ParametersT<DIM,T,S>::ParametersT(const Amg::Vector3D& pos,
				    const Amg::Vector3D& mom,
				    double charge,
				    const S& surface,
				    AmgSymMatrix(DIM)* cov) :  
    ParametersBase<DIM,T>(),
    m_parameters(),
    m_covariance(cov),
    m_position(pos),
    m_momentum(mom),  
    m_surface(surface.isFree() ? surface.clone() : &surface),
    m_chargeDef(charge)
  {
    // m_chargeDef->setCharge(charge);
    
    // get the local parameters via the surface
    Amg::Vector2D lPosition;
    const bool ok = m_surface->globalToLocal(position(),momentum(),lPosition);
    if(not ok)
      lPosition = Amg::Vector2D (INVALID,INVALID);

    // For a neutral particle, last parm should be 1/p rather than q/p.
    double qopnum = this->charge();
    if (qopnum == 0) qopnum = 1;

    // fill the vector now
    m_parameters << lPosition[Trk::loc1], lPosition[Trk::loc2], momentum().phi(),
      momentum().theta(), qopnum/momentum().norm();
  }

  // Constructor with mixed arguments 1 - uses global <-> local for parameters
  template <int DIM, class T, class S> Trk::ParametersT<DIM, T, S>::ParametersT(const Amg::Vector3D& pos,
										double phi, double theta, double qop,
										const S& surface,
										AmgSymMatrix(DIM)* cov):
    ParametersBase<DIM,T>(),
    m_parameters(),
    m_covariance(cov),
    m_position(pos),
    m_momentum(),
    m_surface(surface.isFree() ? surface.clone() : &surface),
    m_chargeDef(1.)
  { 
    // decide the sign of the charge
    if(qop<0.)
      m_chargeDef.setCharge(-1);
    
    // fill momentum & then position using the surface
    double p=0.0;
    if (qop!=0.) {
      p = fabs(1./qop);
    } else {
      // qop is unphysical. No momentum measurement.
      p = INVALID_P;
      qop = INVALID_QOP;
    }
    m_momentum = Amg::Vector3D(p*cos(phi)*sin(theta),
			       p*sin(phi)*sin(theta),
			       p*cos(theta));
    
    // get the local parameters via the surface
    Amg::Vector2D lPosition;
    const bool ok = m_surface->globalToLocal(position(),momentum(),lPosition);
    if (not ok)
      lPosition = Amg::Vector2D (INVALID,INVALID);
    
    // fill the vector now
    // cppcheck-suppress constStatement
    m_parameters << lPosition[Trk::loc1], lPosition[Trk::loc2], phi, theta, qop;   
  }
  
  // Copy constructor
  template<int DIM,class T,class S>
  ParametersT<DIM,T,S>::ParametersT(const ParametersT<DIM,T,S>& rhs):
    ParametersBase<DIM,T>(rhs),
    m_parameters(rhs.m_parameters),
    m_covariance(0),
    m_position(rhs.position()),
    m_momentum(rhs.momentum()),  
    m_surface(nullptr),
    m_chargeDef(rhs.m_chargeDef)
  {

    m_surface.reset((rhs.m_surface && rhs.m_surface->isFree() ? rhs.m_surface->clone() : rhs.m_surface.get()));
    if(rhs.covariance())
      m_covariance = new AmgSymMatrix(DIM)(*rhs.covariance());
  }

  // Move constructor
  template<int DIM,class T,class S>
  ParametersT<DIM,T,S>::ParametersT(ParametersT<DIM,T,S>&& rhs):
    ParametersBase<DIM,T>(std::forward<ParametersBase<DIM,T> >(rhs)),
    m_parameters(std::move(rhs.m_parameters)),
    m_covariance(rhs.m_covariance),
    m_position(std::move(rhs.position())),
    m_momentum(std::move(rhs.momentum())),  
    m_surface(std::move (rhs.m_surface)),
    m_chargeDef(std::move(rhs.m_chargeDef))
  {
    rhs.m_covariance = nullptr;
    rhs.m_surface = nullptr;
  }

  // assignment operator
  template<int DIM,class T,class S>
  ParametersT<DIM,T,S>& ParametersT<DIM,T,S>::operator=(const ParametersT<DIM,T,S>& rhs)
  {
    if(this != &rhs)
    {
      ParametersBase<DIM,T>::operator=(rhs);
     
      // delete the covariance if there
      if(m_covariance) delete m_covariance;
    
      // now assign
      m_parameters = rhs.m_parameters;
      m_covariance = rhs.covariance() ?
        (new AmgSymMatrix(DIM)(*rhs.covariance()))
        : 0;
      m_position   = rhs.position();
      m_momentum   = rhs.momentum();
      m_surface.reset( (rhs.m_surface && rhs.m_surface->isFree()) ? rhs.m_surface->clone() : rhs.m_surface.get());
      m_chargeDef  = rhs.m_chargeDef;
    }
  
    return *this;
  }

  // Move assignment
  template<int DIM,class T,class S>
  ParametersT<DIM,T,S>& ParametersT<DIM,T,S>::operator=(ParametersT<DIM,T,S>&& rhs)
  {
    if(this != &rhs)
    {
      ParametersBase<DIM,T>::operator=(std::move(rhs));

      
      if(m_covariance) delete m_covariance;

      m_parameters = std::move(rhs.m_parameters);
      m_covariance = rhs.m_covariance;
      rhs.m_covariance = nullptr;
      m_position   = std::move(rhs.position());
      m_momentum   = std::move(rhs.momentum());
      m_surface = std::move(rhs.m_surface);
      rhs.m_surface = nullptr;
      m_chargeDef  = std::move(rhs.m_chargeDef);
    }
  
    return *this;
  }

  // Destructor
  template<int DIM,class T,class S>
  ParametersT<DIM,T,S>::~ParametersT()
  {
    if(m_covariance)
    {
      delete m_covariance;
      m_covariance = 0;
    }
  }

 
  /** equality operator */
  template<int DIM,class T,class S>
  bool ParametersT<DIM,T,S>::operator==(const ParametersBase<DIM,T>& rhs) const
  {
    // tolerance for comparing matrices
    static const double& tolerance = 1e-8;

    // make sure we compare objects of same type
    decltype(this) pCasted = dynamic_cast<decltype(this)>(&rhs);
    if(!pCasted)
      return false;

    // comparison to myself?
    if(pCasted == this)
      return true;
    
    // compare parameters
    if(!parameters().isApprox(pCasted->parameters(),tolerance))
      return false;
    
    // compare covariance 
    if(((covariance() != 0) &&
	     (pCasted->covariance() != 0) &&
	     !covariance()->isApprox(*pCasted->covariance(),tolerance))
       || (!covariance() != !pCasted->covariance()))  // <-- this is: covariance() XOR pCast->covariance()
      return false;

    // compare position
    if(!position().isApprox(pCasted->position(),tolerance))
      return false;

    // compare momentum
    if(!momentum().isApprox(pCasted->momentum(),tolerance))
      return false;

    // compare surfaces
    if(associatedSurface() != pCasted->associatedSurface())
      return false;

    // compare charge definition
    if(m_chargeDef != pCasted->m_chargeDef)
      return false;

    // return compatibility of base class parts
    return true;
  }

  // return the measurementFrame
  template<int DIM,class T,class S>
  const Amg::RotationMatrix3D ParametersT<DIM,T,S>::measurementFrame() const
  {
    return associatedSurface().measurementFrame(position(),momentum());
  }

  // protected default constructor
  template<int DIM,class T,class S>
  ParametersT<DIM,T,S>::ParametersT():
    ParametersBase<DIM,T>(),
    m_parameters(),
    m_covariance(nullptr),
    m_position(),
    m_momentum(),
    m_surface(nullptr),
    m_chargeDef()
  {}  

  // protected update function
  template<int DIM,class T,class S>
  void ParametersT<DIM,T,S>::updateParameters(const AmgVector(DIM)& updatedParameters,
                                              AmgSymMatrix(DIM)* updatedCovariance)
  {
    // valid to use != here, because value is either copied or modified,
    bool updatePosition = (updatedParameters[Trk::loc1] != m_parameters[Trk::loc1]) || (updatedParameters[Trk::loc2] != m_parameters[Trk::loc2]);
    bool updateMomentum = (updatedParameters[Trk::phi] != m_parameters[Trk::phi]) || (updatedParameters[Trk::theta] != m_parameters[Trk::theta]) ||
      (updatedParameters[qOverP] != m_parameters[qOverP]);

    // update the parameters vector
    m_parameters = updatedParameters;

    // update the covariance
    if (updatedCovariance){
      if (updatedCovariance != m_covariance)
        delete m_covariance;
      m_covariance = updatedCovariance;
    }
    //position or momentum update needed
    if (updatePosition){
      if(m_surface)
        m_surface->localToGlobal(this->localPosition(),m_momentum,m_position);
      else
      {
        m_momentum.setZero();
        m_position.setZero();
      }
    } 
    // momentum update is needed    
    if (updateMomentum){
      double phi   = m_parameters[Trk::phi];
      double theta = m_parameters[Trk::theta];
      double p = INVALID_P;
      if (m_parameters[Trk::qOverP] != 0)
        p = this->charge()/m_parameters[Trk::qOverP];
      m_momentum = Amg::Vector3D(p*cos(phi)*sin(theta),p*sin(phi)*sin(theta),p*cos(theta));
    }
  }

//special C-tor used by the curvilinear parameters  
  template <int DIM,class T,class S>
  Trk::ParametersT<DIM,T,S>::ParametersT(const Amg::Vector3D& pos,
					 const Amg::Vector3D& mom,
					 AmgSymMatrix(DIM)* cov):
    m_parameters(),
    m_covariance(cov),
    m_position(pos),
    m_momentum(mom),
    m_surface(nullptr),
    m_chargeDef()
  {}
 
  // Protected Constructor with local arguments - persistency only, ownership of surface given
  template <int DIM,class T,class S>
  Trk::ParametersT<DIM,T,S>::ParametersT(const AmgVector(DIM)& pars,
                                         const S* surface,
                                         AmgSymMatrix(DIM)* cov) :
    ParametersBase<DIM,T>(),
    m_parameters(pars),
    m_covariance(cov),
    m_surface(surface)
  {
    float qop = m_parameters[Trk::qOverP];
    // decide the sign of the charge
    if (qop<0.) m_chargeDef.setCharge(-1);

    double p=0.0;
    if (qop!=0.) {
      p = fabs(1./qop);
    } else {
      // qop is unphysical. No momentum measurement.
      p = INVALID_P;
      qop = INVALID_QOP;
    }
    // fill momentum & then position using the surface
    m_momentum = Amg::Vector3D(p*cos(m_parameters[Trk::phi])*sin(m_parameters[Trk::theta]),
			       p*sin(m_parameters[Trk::phi])*sin(m_parameters[Trk::theta]),
			       p*cos(m_parameters[Trk::theta]));
    if (m_surface)
      m_surface->localToGlobal(this->localPosition(),m_momentum,m_position);
    else {
      m_momentum.setZero();
      m_position.setZero();
    }
  }
 
  
} // end of namespace Trk
